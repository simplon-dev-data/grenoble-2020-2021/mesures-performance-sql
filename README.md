# Mesures de performance et optimisation des requêtes SQL

> Mesurer et optimiser le temps d'exécution des requêtes SQL

Nous allons travailler à partir du jeu "Demandes de valeurs foncières géolocalisées"
par Etalab:

- URL: https://www.data.gouv.fr/fr/datasets/demandes-de-valeurs-foncieres-geolocalisees/
- CSV: https://www.data.gouv.fr/fr/datasets/r/316795eb-a3fa-465d-b058-38ef8579da11

Ce jeu de données est intéressant car sur les années complètes (par exemple 2019),
les données brutes pèsent environ 500 Mo pour environ 3 millions de lignes !

Donc faire des requêtes d'exploitation sur un tel jeu de données peu
prendre du temps, beaucoup de temps ... 💤

## Documentation

Avant toute chose : le domaine de l'optimisation de performance est très vaste !
Toutes les techniques et solutions dépendent très fortement :

- Du jeu de données (cohérence, qualité, quantité)
- Du type d'exploitation (application, BI)
- Du moteur de base de données (SQLite, PostgreSQL, SQL Server, etc.)
- Du matériel utilisé (serveur, cluster, etc.)
- Et de pleins d'autres facteurs !

Pour référence, quelques liens de documentation pour PostgreSQL :

- https://www.postgresql.org/docs/current/performance-tips.html
- https://www.postgresql.org/docs/current/sql-explain.html
- https://thoughtbot.com/blog/postgresql-performance-considerations
- https://thoughtbot.com/blog/reading-an-explain-analyze-query-plan
- https://thoughtbot.com/blog/advanced-postgres-performance-tips
- https://www.geekytidbits.com/performance-tuning-postgres/
- https://explain.depesz.com/

## Chargement du jeu de données

Nous allons charger notre jeu de données brutes dans notre entrepôt de données :

1. Lancer Postgres et Metabase:

```bash
sudo docker-compose up -d
```

2. Une fois Postgres disponible, lancer le pipeline ETL
   (patience, cela peut prendre entre 20 et 30 mn !) :

```bash
bash pipeline.sh
```

3. Une fois le pipeline terminé, vérifier les données dans Postgres
   (il est important de noter la taille originale des données !) :

```bash
sudo docker-compose exec postgres psql -h postgres -U postgres dvf
Password for user postgres: test
psql (13.1 (Debian 13.1-1.pgdg100+1))
Type "help" for help.

dvf=# \d+
                            List of relations
 Schema |  Name   | Type  |  Owner   | Persistence |  Size  | Description
--------+---------+-------+----------+-------------+--------+-------------
 public | dvf_raw | table | postgres | permanent   | 553 MB |
(1 row)
```

## Activités

Vous trouverez plusieurs activités à réaliser dans ce projet.
Elles sont par ordre croissant de difficulté :

- [Activité 1](ACTIVITE_1.md) : découvrir la mesure de performance
  et l'optimisation de requêtes SQL
- [Activité 2](ACTIVITE_2.md) : optimiser l'exploitation des données
  par un outil BI

Nous rajouterons très probablement d'autres activités au cours du temps.
