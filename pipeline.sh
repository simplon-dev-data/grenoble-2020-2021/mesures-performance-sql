#!/bin/bash

set -ex

DATA_PATH=data
CSV_GZ_URL=https://cadastre.data.gouv.fr/data/etalab-dvf/latest/csv/2019/full.csv.gz
CSV_GZ_PATH="${DATA_PATH}/dvf.csv.gz"
CSV_UNGZ_PATH="${DATA_PATH}/full.csv"
CSV_PATH="${DATA_PATH}/dvf.csv"

function initialize() {
    mkdir -p ${DATA_PATH}
}

function extract() {
    curl -o ${CSV_GZ_PATH} ${CSV_GZ_URL}
    gzip -f -d ${CSV_GZ_PATH}
}

function load() {
    docker-compose exec -T -e PGPASSWORD=test postgres psql -h postgres -U postgres dvf < sql/warehouse.sql
    docker-compose exec -T -e PGPASSWORD=test postgres psql -h postgres -U postgres dvf < sql/load.sql
}

function cleanup() {
    rm -rf ${DATA_PATH}
}

initialize
extract
load
cleanup
