CREATE TABLE IF NOT EXISTS dvf_raw (
    "id_mutation" VARCHAR(16),
    "date_mutation" DATE,
    "numero_disposition" VARCHAR(8),
    "nature_mutation" VARCHAR(128),
    "valeur_fonciere" REAL,
    "adresse_numero" INTEGER,
    "adresse_suffixe" VARCHAR(8),
    "adresse_nom_voie" VARCHAR(128),
    "adresse_code_voie" VARCHAR(8),
    "code_postal" VARCHAR(8),
    "code_commune" VARCHAR(8),
    "nom_commune" VARCHAR(128),
    "code_departement" VARCHAR(3),
    "ancien_code_commune" VARCHAR(8),
    "ancien_nom_commune" VARCHAR(128),
    "id_parcelle" VARCHAR(16),
    "ancien_id_parcelle" VARCHAR(16),
    "numero_volume" VARCHAR(8),
    "lot1_numero" VARCHAR(8),
    "lot1_surface_carrez" REAL,
    "lot2_numero" VARCHAR(8),
    "lot2_surface_carrez" REAL,
    "lot3_numero" VARCHAR(8),
    "lot3_surface_carrez" REAL,
    "lot4_numero" VARCHAR(8),
    "lot4_surface_carrez" REAL,
    "lot5_numero" VARCHAR(8),
    "lot5_surface_carrez" REAL,
    "nombre_lots" INTEGER,
    "code_type_local" INTEGER,
    "type_local" VARCHAR(128),
    "surface_reelle_bati" INTEGER,
    "nombre_pieces_principales" INTEGER,
    "code_nature_culture" VARCHAR(8),
    "nature_culture" VARCHAR(128),
    "code_nature_culture_speciale" VARCHAR(128),
    "nature_culture_speciale" VARCHAR(128),
    "surface_terrain" INTEGER,
    "longitude" REAL,
    "latitude" REAL
);
