# Activité 2

> Optimiser l'exploitation des données par un outil BI

L'objectif de cette activité est de se confronter à un scénario classique :

- Un analyste créé un dashboard d'exploitation des données
- Le dashboard effectue beaucoup de requêtes sur une importante quantité de données
- L'exploitation par la visualisation de données est impactée par les pauvres
  performances de la base de données
- Il faut trouver comment rendre l'exploitation des données performante

## Metabase

Lancer Metabase (http://localhost:3000) et se connecter avec le compte administrateur :

- E-mail : `john@doe.com`
- Password : `simplon123`

Si besoin, re-configurer l'accès à la base de données Postgres :

- Hostname : `postgres`
- Port : `5432`
- Username : `postgres`
- Password : `test`
- Database : `dvf`

La base de données interne de Metabase vous est fournie. Vous devez voir apparaître
un dashboard sur la page d'accueil. Prenez le temps d'utiliser le dashboard
pour vous rendre compte des problèmes de performance.

## Enquête

Vous êtes en charge d'améliorer la performance du dashboard Metabase.

Grâce à la méthodologie initiée dans l'activité 1, essayer de comprendre
quelles visualisations posent le plus problème et tentez de trouver
quelques optimisations.

Notez toutes vos observations (mesures, etc.) et les actions misent en place
avec les résultats obtenus à chaque étape.

### Observations et actions

`[A COMPLETER]`
