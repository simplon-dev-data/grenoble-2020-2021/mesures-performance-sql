# Activité 1

> Découvrir la mesure de performance et l'optimisation de requêtes SQL

Faisons nos premiers pas en analyse du temps d'exécution de requêtes SQL,
et quelles techniques employer pour réduire ce temps lorsque c'est nécessaire.

## Objectif

Supposons que l'on souhaite connaître "la moyenne de la valeur foncière des
maisons par commune". La requête SQL associée est :

```sql
SELECT
    "nom_commune",
    AVG("valeur_fonciere") as "moyenne_valeur_fonciere"
FROM dvf_raw
WHERE "type_local" = 'Maison'
GROUP BY "nom_commune"
ORDER BY "moyenne_valeur_fonciere" DESC;
```

Nous allons tenter de mesurer la performance de cette requête
et de trouver une optimisation possible améliorant sa
vitesse d'exécution.

## Mesures de performance

Dans un premier temps, nous allons nous focaliser sur la mesure du temps
d'exécution de requêtes SQL à partir du jeu de données brutes, **sans optimisations**.

**Il est primordial d'effectuer des mesures de performance AVANT de tenter une optimisation !**
Ces mesures serviront de références pour vérifier après coup si une optimisation
est réellement efficace.

### Activation de la mesure de temps Postgres

Afin de pouvoir mesurer le temps d'exécution de chaque requête dans Postgres,
il est nécessaire d'activer le mode `timing` :

```sql
dvf=# \timing
Timing is on.
```

Ensuite il suffit de lancer une requête pour obtenir le temps d'exécution.
Par exemple, pour compter le nombre total de ventes concernant la ville de Grenoble
dans la table `dvf_raw` :

```sql
dvf=# SELECT COUNT(*) FROM dvf_raw WHERE nom_commune = 'Grenoble';
 count
-------
  3714
(1 row)

Time: 406.942 ms
```

La requête a pris environ 400 millisecondes pour s'exécuter.

**À FAIRE** :

- [ ] Mesurer le temps d'exécution de la requête `SELECT COUNT(*) FROM dvf_raw;`
- [ ] Mesurer le temps d'exécution de la requête de l'objectif
- [ ] Mesurer le temps de la requête pour plusieurs exécutions successives
- [ ] Mesurer le temps de la requête sur des machines différentes

### Analyse du _plan d'exécution_ (_query plan_)

Comme vous avez pu le constater, il est difficile d'obtenir une mesure constante
du temps d'exécution d'une requête : cela vient notamment du fait que les moteurs
de base de données optimisent fortement les calculs et se permettent de réutiliser
des informations précédemment calculées pour gagner de précieuses secondes !

Pour obtenir des mesures plus fiables, nous allons utiliser le _plan d'exécution_ (_query plan_).
En plus de nous fournir une mesure du temps d'exécution plus objective, cela va
également nous donner le détail du chemin d'exécution d'une requête par le moteur.

Avec PostgreSQL, la command `EXPLAIN ANALYZE` nous permet d'obtenir le plan d'exécution :

```sql
dvf=# EXPLAIN ANALYZE SELECT COUNT(*) FROM dvf_raw WHERE nom_commune = 'Grenoble';
                                    QUERY PLAN
-------------------------------------------------------------------------------
 Finalize Aggregate  (cost=88165.37..88165.38 rows=1 width=8) (actual time=485.351..486.635 rows=1 loops=1)
   ->  Gather  (cost=88165.16..88165.37 rows=2 width=8) (actual time=477.405..486.552 rows=3 loops=1)
         Workers Planned: 2
         Workers Launched: 2
         ->  Partial Aggregate  (cost=87165.16..87165.17 rows=1 width=8) (actual time=438.182..438.230 rows=1 loops=3)
               ->  Parallel Seq Scan on dvf_raw  (cost=0.00..87161.23 rows=1572 width=0) (actual time=301.402..413.327 rows=1238 loops=3)
                     Filter: ((nom_commune)::text = 'Grenoble'::text)
                     Rows Removed by Filter: 1046413
 Planning Time: 0.469 ms
 Execution Time: 487.216 ms
(10 rows)

Time: 488.882 ms
```

Un plan d'exécution se lit **en profondeur** : ici l'ordre des opérations à effectuer est :

1. "Parallel Seq Scan"
2. "Partial Aggregate"
3. "Gather"
4. "Finalize Aggregate".

Dans l'exemple ci-dessus, on peut observer que l'opération "Parallel Seq Scan"
semble prendre le plus de temps : appliquer en premier le filtre de nom de commune
sur la table entière est nécessaire pour calculer le total du nombre de lignes !

Il est compliqué de comprendre un plan d'exécution mais généralement cela permet :

- D'observer le temps passé sur chaque partie de la requête (filtrage, groupements, tri, aggrégations)
- De focaliser les efforts sur les parties prenant le plus de temps d'exécution.

**À FAIRE** :

- [ ] Analyser le plan d'exécution de la requête `SELECT COUNT(*) FROM dvf_raw;`
- [ ] Analyser le plan d'exécution de la requête de l'objectif
- [ ] Essayer de déduire quelle(s) opérations prennent le plus de temps pour
      la requête de l'objectif

## Optimisations

**Avertissement** : il n'existe pas de recette miracle ! L'art de l'optimisation
se maîtrise avec l'expérimentation et l'expérience. Néanmoins, une bonne analyse
de la situation problématique peut orienter vers des solutions plus adéquates.
Dans tous les cas, toute optimisation est une "balance" et le gain se fera au
détriment d'autre chose : stockage disque, temps d'écriture plus long,
développements supplémentaires, préparation de données, etc.

Les actions d'optimisation les plus répandue d'un SGBD sont :

- L'utilisation des bons types de données
- L'utilisation des _jointures internes_ (_inner joins_)
  au lieu des _jointures externes_ (_outer joins_)
- La création d'_indices_ (_indexes_)
- La création de _vues matérialisées_ (_materialized views_)

### Création d'indice

#### Action

Dans notre exemple précédent, l'opération la plus coûteuse était le filtre sur
le nom de commune (intuitivement puis confirmé par le plan d'exécution).
Essayons d'ajouter un indice pour la colonne `dvf_raw.nom_commune` afin d'accélérer
l'opération de filtrage sur cette colonne :

```sql
dvf=# CREATE INDEX ON dvf_raw(nom_commune);
CREATE INDEX
Time: 44457.808 ms (00:44.458)
```

Comme on peut le constater, la création d'un indice peut prendre du temps
(ici, environ 45 secondes) !

#### Mesure

Essayons de voir si notre optimisation a permis de diminuer le temps d'exécution
de la requête :

```sql
dvf=# EXPLAIN ANALYZE SELECT COUNT(*) FROM dvf_raw WHERE nom_commune = 'Grenoble';
                                    QUERY PLAN
-------------------------------------------------------------------------------
 Aggregate  (cost=91.87..91.88 rows=1 width=8) (actual time=103.468..103.501 rows=1 loops=1)
   ->  Index Only Scan using dvf_raw_nom_commune_idx on dvf_raw  (cost=0.43..82.44 rows=3772 width=0) (actual time=12.319..60.476 rows=3714 loops=1)
         Index Cond: (nom_commune = 'Grenoble'::text)
         Heap Fetches: 0
 Planning Time: 8.215 ms
 Execution Time: 103.593 ms
(6 rows)

Time: 112.656 ms
```

Le temps d'exécution de la requête est passé de 487 à 103 millisecondes :
nous avons divisé le temps (ou augmenté la performance) par environ 4.7 !
Le plan d'exécution de la requête nous montre également que désormais seulement
2 opérations sont nécessaires : le filtrage se fait grâce à notre indice,
ce qui signifie qu'il n'y a plus besoin de parcourir la table entièrement
pour trouver les lignes concernées, d'où le gain de performance !

#### Effet de bord

Quelle est la contre-partie de cette action d'optimisation ?
Observons la taille de stockage occupée par ce nouvel indice :

```sql
dvf=# \di+
                                         List of relations
 Schema |          Name           | Type  |  Owner   |  Table  | Persistence | Size  | Description
--------+-------------------------+-------+----------+---------+-------------+-------+-------------
 public | dvf_raw_nom_commune_idx | index | postgres | dvf_raw | permanent   | 22 MB |
(1 row)
```

L'indice de la colonne `dvf_raw.nom_commune` occupe 22 Mo. Rappelez-vous,
une optimisation a toujours un coût : ici du stockage supplémentaire !

**À FAIRE** :

- [ ] À partir du plan d'exécution de la requête de l'objectif,
      essayer de mettre en place une mesure d'optimisation
- [ ] Mesurer l'effet de l'action d'optimisation
- [ ] Trouver la contre-partie de l'action d'optimisation

### Utilisation d'une vue matérialisée

Changeons maintenant d'exemple et reprenons une requête simple mais qui ne peut
être optimisée seulement avec la création d'indices :

```sql
SELECT COUNT(*) FROM dvf_raw;
```

Une rapide mesure de la requête :

```sql
dvf=# EXPLAIN ANALYZE SELECT COUNT(*) FROM dvf_raw;
                                    QUERY PLAN
---------------------------------------------------------------------------------
 Finalize Aggregate  (cost=88157.76..88157.77 rows=1 width=8) (actual time=35410.588..35417.777 rows=1 loops=1)
   ->  Gather  (cost=88157.54..88157.75 rows=2 width=8) (actual time=35409.074..35417.704 rows=3 loops=1)
         Workers Planned: 2
         Workers Launched: 2
         ->  Partial Aggregate  (cost=87157.54..87157.55 rows=1 width=8) (actual time=35329.702..35329.733 rows=1 loops=3)
               ->  Parallel Seq Scan on dvf_raw  (cost=0.00..83883.63 rows=1309563 width=0) (actual time=1.562..17836.410 rows=1047651 loops=3)
 Planning Time: 0.364 ms
 Execution Time: 35417.963 ms
```

#### Action

Une mesure d'optimisation beaucoup plus radicale consiste à créer une _vue matérialisée_ :
nous allons demander à PostgreSQL de créer un alias d'une requête SQL et de stocker son
résultat en mémoire.

**Avantage** : dès que nous nous servirons de cette nouvelle entité, le résultat sera accessible
instantanément !

**Inconvénient** : le résultat n'est plus dynamique automatiquement si les données changent,
nous devons déclencher "manuellement" un rafraîchissement.

Mettons cela en pratique :

```sql
CREATE MATERIALIZED VIEW count_all_dvf AS
SELECT COUNT(*) FROM dvf_raw;
```

Pour se servir de la vue matérialisé, il suffit d'exécuter :

```sql
SELECT * FROM count_all_dvf;
```

#### Mesure

Mesurons le gain de performance observé :

```sql
dvf=# EXPLAIN ANALYZE SELECT * FROM count_all_dvf;
                                  QUERY PLAN
-------------------------------------------------------------------------------
 Seq Scan on count_all_dvf  (cost=0.00..32.60 rows=2260 width=8) (actual time=0.018..0.035 rows=1 loops=1)
 Planning Time: 4.604 ms
 Execution Time: 0.152 ms
```

Le résultat est sans appel : nous sommes passés de 35 secondes à 0.15 millisecondes !
(un gain de performance d'environ 233x !)

#### Effet de bord

Observons les caractéristiques de l'entité créée :

```sql
dvf=# \dm+
                                       List of relations
 Schema |     Name      |       Type        |  Owner   | Persistence |    Size    | Description
--------+---------------+-------------------+----------+-------------+------------+-------------
 public | count_all_dvf | materialized view | postgres | permanent   | 8192 bytes |
```

L'impact mémoire semble totalement négligeable.

En revanche, comme évoqué plus haut, si le nombre de lignes change dans la
table `dvf_raw`, le résultat retourné ne sera pas mis à jour automatiquement !

Afin de mettre à jour notre vue matérialisée, il est nécessaire de la rafraîchir
manuellement :

```sql
REFRESH MATERIALIZED VIEW count_all_dvf;
```

Il est également possible de déclencher "automatiquement" une mise à jour d'une
vue matérialisée en utilisant à bon escient le concept de _déclencheur_ (_trigger_)
mais c'est une fonctionnalité plus avancée ...
